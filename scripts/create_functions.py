def is_palindrome(string):
    string = string.replace(" ", "").lower()
    print(string)
    return string == string[::-1]


def is_pangram(string):
    is_letter = True
    count = 0
    for letter in string:
        if letter in "abcdefghijklmnopqrstuvwxyz":
            count += 1
    return count == len(string)


def no_duplicates(a_list):
    if a_list != list(a_list):
        print("No Duplicates takes only a list []")
    a_list = set(a_list)
    return list(a_list)


def add_values_in_list(a_list):
    if a_list != list(a_list):
        print("No Duplicates takes only a list []")
    number = 0

    for num in a_list:
        number += num

    return number


def max2(a, b):
    return a if a > b else b


def max3(a, b, c):
    max_of_two = max2(a, b)
    return c if c > max_of_two else max_of_two


print("The max value of 1, 3, 5 is:")
print(max3(1, 5, 3))
# print(max2(1, 2))
print("\nRemoving duplicates in [1, 1, 3, 4, 5, 5]")
print(no_duplicates([1, 1, 3, 4, 5, 5]))
print("\nThe total of 1+3+8 is:")
print(add_values_in_list([1, 3, 8]))

print("\nIs the word laptop a pangram")
print(is_pangram("laptop"))

print("\nIs the word laptop5 a pangram")
print(is_pangram("laptop5"))

print("\nIs the following a palindrome")
print(is_palindrome("racecar"))

print("\nIs the following a palindrome")
print(is_palindrome("beetle"))

print("\nIs the following a palindrome")
print(is_palindrome("Ten animals I slam in a net"))
