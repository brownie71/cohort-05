import random

rand_num = str(random.randint(1000, 9999))

print(rand_num)
tries = 5
got_input = True

while got_input or tries != 0:
    guess = input("Please guess 4 number: ")

    if len(guess) != len(rand_num):
        print("Sorry, your guess was not 4 numbers")
        guess = input("Please try again: ")

    if guess == rand_num:
        print(f"You win, the number was {rand_num}")
        break
    bulls = 0
    cows = 0
    x = 0
    for g in guess:
        if g in rand_num[::1] and g == rand_num[x]:
            bulls += 1

        elif g in rand_num[::1] and g != rand_num[x]:
            cows += 1

        x += 1
    tries -= 1

    print(f"Bulls = {bulls}")
    print(f"Cows = {cows}")
else:
    print("Sorry, better luck next time!!!!")
