

# Modify the shell script you just wrote so that it takes multiple command line arguments and checks for existence of all of them, rather than just one
# While you're at it, append a line to each file that says "inspected by "
# ...and, just for fun, remove files that are completely empty

if [[ $# -lt 2 ]]; then
    echo "------------------------------------------"
    echo "Please add ARGS <create> <name of branch>"
    echo "                <update> <comment>"
    echo "                <push>   <name of branch>"
    echo "                <delete> <name of branch>"
    echo "------------------------------------------"
    # exit 1
fi

action="$1"
branch_name="$2"
comment="$3"

# create a new branch
if [[ $# -eq 2 ]] && [[ $action = 'create' ]]; then
    echo "Creating new branch named: "$branch_name
    git checkout -b $branch_name
    branch_command=`git rev-parse --abbrev-ref HEAD`
    if [[ $branch_command = $branch_name ]]; then
        echo "You are now in branch "$branch_name; 
    fi
    exit 0
fi

# Update a branch
if [[ $# -eq 2 ]] && [[ $action = 'update' ]]; then
    git add .
    git commit -m \""$branch_name\""
    echo "Updated branch "$branch_name
    exit 0
fi

# Push branch to master
if [[ $# -eq 2 ]] && [[ $action = 'push' ]]; then
    git push origin $branch_name
    echo "branch \"$branch_name\" pushed to origin"
    exit 0
fi

# Delete a branch
if [[ $# -eq 2 ]] && [[ $action = 'delete' ]]; then
    git checkout master
    if [[ $branch_command = "master" ]]; then
        echo "Master checked out"
        echo "git branch -d $branch_name"
        git branch -d $branch_name
        if [[ ! $? ]]; then
            git branch -D $branch_name
        fi
        echo "branch \"$branch_name\" deleted"
        exit 0
    
    fi

fi
