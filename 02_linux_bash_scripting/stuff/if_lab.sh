

# Modify the shell script you just wrote so that it takes multiple command line arguments and checks for existence of all of them, rather than just one
# While you're at it, append a line to each file that says "inspected by "
# ...and, just for fun, remove files that are completely empty

if [[ $# -lt 1 ]]; then
    echo "Please add a filename"
fi

# Check for file input argument
for var in "$@"; do
    if [[ ! var ]]; then
        echo " "
        echo "Please enter a filename!"
        echo " "
        echo $(ls -l)
        exit 1
    elif [[ ! -f  ${var} ]]; then
        echo " "
        echo "Your ${var} file doesn't exist.  Please add your file on the command line!"
        echo " "
        exit 1
    elif [ ! -s $var ]; then
        rm $var
        echo "${var} is empty and has been removed"
    else
        echo "Inspected by JB " $(date) >> $var
    fi
done

# Get the filename
filename="$1"

# Check if file exists
# if [[ ! -f  ${filename} ]]; then
# 	echo " "
#     echo "Your ${filename} file doesn't exist.  Please add your file on the command line!"
# 	echo " "
#     exit 1
# fi

# scriptname=$0
# echo "Script ${scriptname##*/} inspected by ${USER##*\\}"

