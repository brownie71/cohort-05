
ls | grep -P '[a-zA-z0-9\-\ ]+.(png|jpg|gif)' > files_to_move.txt

while read -r line; do
    echo "Text read from file: $line"
    mv "$line" png
done < files_to_move.txt