from fizzbuzz import fizzbuzz
import pytest


def test_fizzbuzz_one():
    value = 1
    result = fizzbuzz(value)
    assert result == 1


def test_fizzbuzz_two():
    value = 2
    result = fizzbuzz(value)
    assert result == 2


def test_fizzbuzz_three():
    value = 3
    result = fizzbuzz(value)
    assert result == "fizz"


def test_fizzbuzz_five():
    value = 5
    result = fizzbuzz(value)
    assert result == "buzz"


def test_fizzbuzz_six():
    value = 6
    result = fizzbuzz(value)
    assert result == "fizz"


def test_fizzbuzz_ten():
    value = 5
    result = fizzbuzz(value)
    assert result == "buzz"


def test_fizzbuzz_fifteen():
    value = 15
    result = fizzbuzz(value)
    assert result == "fizzbuzz"


def test_fizzbuzz_string():
    value = "something"
    with pytest.raises(TypeError):
        result = fizzbuzz(value)


def test_fizzbuzz_float():
    value = 1.22
    result = fizzbuzz(value)
    assert result == value


def test_fizzbuzz_negative():
    value = -4
    result = fizzbuzz(value)
    assert result == value
