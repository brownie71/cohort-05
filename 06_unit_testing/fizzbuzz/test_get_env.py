from unittest.mock import patch
import pytest
from get_env import get_env
import os


def test_get_env(monkeypatch):
    monkeypatch.setenv("DATABASE_HOST", "aws.rds.lkjjasdf")
    assert get_env() == "mysql://aws.rds.lkjjasdf"
    assert get_env() != "mysql:/aws.rds.lkjjasdf"
