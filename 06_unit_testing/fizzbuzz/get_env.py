import os


def get_env():
    db_string = os.getenv("DATABASE_HOST")
    return "mysql://" + db_string
