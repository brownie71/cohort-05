import requests

from bs4 import BeautifulSoup as Soup


stocks = [
    "fb",
    "f",
    "bidu",
    "msft",
    "tsla",
    "amzn",
    "spot",
    "eth-usd",
    "btc-usd",
]  # STOCK LIST
print("\n" + ("-" * 25))

for stock in stocks:
    # symbol = input("Enter stock symbol ")

    r = requests.get("https://finance.yahoo.com/quote/" + stock)

    page_soup = Soup(r.text, "html.parser")

    price = [
        entry.text
        for entry in page_soup.find_all(
            "span", {"class": "Trsdu(0.3s) Fw(b) Fz(36px) Mb(-4px) D(ib)"}
        )
    ]
    company_name = page_soup.find("h1", {"class": "D(ib) Fz(18px)"})

    neg_movement = [
        entry.text
        for entry in page_soup.find_all(
            "span",
            {"class": "Trsdu(0.3s) Fw(500) Pstart(10px) Fz(24px) C($negativeColor)"},
        )
    ]
    pos_movement = [
        entry.text
        for entry in page_soup.find_all(
            "span",
            {"class": "Trsdu(0.3s) Fw(500) Pstart(10px) Fz(24px) C($positiveColor)"},
        )
    ]

    if neg_movement != []:
        neg_movement = "".join(neg_movement).split()
        movement = neg_movement if neg_movement == [] else neg_movement[0]
    if pos_movement != []:
        pos_movement = "".join(pos_movement).split()
        movement = pos_movement if pos_movement == [] else pos_movement[0]

    print(company_name.text)
    print("Current Price: $", "".join(price), "".join(movement))

    print("-" * 25)

    # print("-" * 50)
    # news =  [news_entry.text for news_entry in page_soup.find_all("li", {"class":"js-stream-content Pos(r)"})]
    # for index, n in enumerate(news):
    #     print(index + 1, n, "\n")
