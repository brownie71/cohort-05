import random
import os

"""
USER STARTS GAME
    RANDOM WORD IS GENERATED
    BLANK SPACES TAKE THE PLACE OF THE WORD
        USER IS PROMPTED TO GUESS A LETTER
        USERS LETTER GUESS IS COMPARED TO LETTERS IN RANDOM WORD
            IF WRONG
                LETTER IS PUT IN WRONG LETTERS LIST
                USER GUESSES AGAIN
            ELSE
                LETTER REPLACES A BLANK
                USER GUESS AGAIN
        WHEN WORD IS COMPLETE GAME ENDS
"""


def underscore(rand_word):
    hidden_word = []

    i = 0
    while i < len(rand_word):
        hidden_word.append("_")
        i += 1

    return hidden_word


def randomWordGenerator(WORDS):
    rand_word = random.choice(WORDS).lower()
    return rand_word


HANGMAN_PICS = [
    """
  +---+
      |
      |
     ===""",
    """
  +---+
  O   |
      |
      |
     ===""",
    """
  +---+
  O   |
  |   |
      |
     ===""",
    """
  +---+
  O   |
 /|   |
      |
     ===""",
    """
  +---+
  O   |
 /|\  |
      |
     ===""",
    """
  +---+
  O   |
 /|\  |
 /    |
     ===""",
    """
  +---+
  O   |
 /|\  |
 / \  |
     ===""",
]

# BLACK WILL REDUCE LINE SIZE
WORDS = [
    "Accio",
    "Alchemy",
    "Alohomora",
    "Animagus",
    "Auror",
    "Azkaban",
    "Basilisk",
    "Bludgers",
    "Bowtruckle",
    "Butterbeer",
    "Charm",
    "Chimaera",
    "Chocoballs",
]

# rand_word = random.choice(WORDS).lower()
missed_letters = []
correct_letters = []

print("HARRY POTTER HANGMAN\n")

# CHOOSES A RANDOM WORD FROM WORDS LIST
rand_word = randomWordGenerator(WORDS)
print(rand_word)

# Prints the initial image
print(HANGMAN_PICS[0])

# CONSTRUCTS THE UNDERSCORE FOR THE RANDOM WORD
blanks = underscore(rand_word)
print(" ".join(blanks))

# ITERATOR FOR HANGMAN IMAGE
x = 0


while True:
    users_guess = input("\nPlease guess a letter: ").lower()

    if users_guess == "quit":
        break
    i = 0
    if users_guess in rand_word[::1] and len(users_guess) == 1:

        if users_guess in correct_letters:
            os.system("clear")
            # print("\nCorrect Letters", ", ".join(correct_letters), '|', "Missed Letters", ", ".join(missed_letters))
            print(
                f"THE LETTER < {users_guess} > HAS ALREADY BEEN SELECTED, PLEASE TRY AGAIN"
            )
        else:
            correct_letters.append(users_guess)

        for letter in rand_word:
            if users_guess == letter:
                blanks[i] = letter

            i += 1
        os.system("clear")

        print(" ".join(blanks))
        print(HANGMAN_PICS[x])

        if "".join(blanks) == rand_word:
            print((rand_word).upper(), "is correct!!! YOU WIN!!!!")
            break

    elif len(users_guess) == 1:

        # os.system('clear')

        print(" ".join(blanks))
        if x < (len(HANGMAN_PICS) - 1):
            os.system("clear")

            if users_guess in missed_letters:
                print(HANGMAN_PICS[x])
                print(
                    "\nCorrect Letters",
                    ", ".join(correct_letters),
                    "|",
                    "Missed Letters",
                    ", ".join(missed_letters),
                )
                print("THAT LETTER HAS ALREADY BEEN SELECTED, PLEASE TRY AGAIN")
                continue
            else:
                x += 1
                print(" ".join(blanks))
                missed_letters.append(users_guess)
                print(HANGMAN_PICS[x])
        else:
            print("GAME OVER")
            break

    else:
        os.system("clear")

        print(" ".join(blanks))
        print(HANGMAN_PICS[x])
        print("PLEASE ENTER ONLY 1 LETTER!!")

    print(
        "\nCorrect Letters",
        ", ".join(correct_letters),
        "|",
        "Missed Letters",
        ", ".join(missed_letters),
    )


# # BLACK WILL REDUCE LINE SIZE
# WORDS = ["Accio","Alchemy","Alohomora","Animagus","Auror","Azkaban","Basilisk","Bludgers","Bowtruckle","Butterbeer","Charm","Chimaera","Chocoballs",]

# # rand_word = random.choice(WORDS).lower()
# missed_letters= []
# correct_letters =[]

# print('HARRY POTTER HANGMAN\n')

# # CHOOSES A RANDOM WORD FROM WORDS LIST
# rand_word =  randomWordGenerator(WORDS)
# print(rand_word)
