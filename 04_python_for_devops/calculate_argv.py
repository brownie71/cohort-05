import sys


def calculate(a, b, operator):
    a = int(a)
    b = int(b)
    if operator == "+":
        return a + b
    elif operator == "-":
        return a - b
    elif operator == "*":
        return a * b
    elif operator == "/":
        return a / b


if len(sys.argv) == 4 and sys.argv[3] in "+-*/":
    # print('Program arguments', sys.argv)
    calc = calculate(sys.argv[1], sys.argv[2], sys.argv[3])
    print(calc)
else:
    # print('Program arguments', sys.argv)
    print("Only 3 arguments required")
